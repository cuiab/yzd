$(function () {
	var active_class = 'active';

	$('.tables > .chex > .chex0 > .pos-rel> input[type=checkbox]').eq(0).on('click', function() {
//		console.log($('.tables > .chex > .chex0 >.pos-rel > input[type=checkbox]'));
		var th_checked = this.checked; //checkbox inside "TH" table header
	
		$(this).closest('.tables').find('.chex1,.chex2,.chex3').each(function() {
			var row = this;
			if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
			else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
		});
	});
})
		