
//$(function () {
//	var active_class = 'active';
//
//	$('#dynamic-table > thead > tr > th > label > input[type=checkbox]').eq(0).on('click', function() {
//		console.log($('#dynamic-table > thead > tr > th input[type=checkbox]'));
//		var th_checked = this.checked; //checkbox inside "TH" table header
//	
//		$(this).closest('table').find('tbody > tr').each(function() {
//			var row = this;
//			if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
//			else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
//		});
//	});
//	
//})

$(function () {
	$('#dynamic-table').on('click', 'td :checkbox', function() {
		var $row = $(this).closest('tr');
		if(this.checked) $row.addClass('active');
		else $row.removeClass('active');
	}).on('click','th:eq(0) :checkbox', function(){
		var checkall = this.checked;
		$(this).closest('table').find('tbody :checkbox').each(function(){
			this.checked = checkall;
			var $row = $(this).closest('tr');
			if(this.checked) $row.addClass('active');
			else $row.removeClass('active');
		});
	});
})
	


