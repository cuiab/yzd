function TabControl(labelWrapper, contentWrapper) {
    var self = this;
    
    self.add = function (opts) {
        var $tabWrapper = $(labelWrapper);
        var tabsCnt = $tabWrapper.children().length;
        var options = $.extend({
            id: 'labels id' + tabsCnt,
            title: 'labels title',
            url: 'contents url',
            onLoadSuccess: function () {}
        }, opts);
    
        var existTab = $tabWrapper.children().children('[href="#tab-' + options.id + '"]');
        if (existTab.length > 0) {
            return existTab.trigger('click');
        }
        
        var $label = $('<li>' +
            '<a data-toggle="tab" href="#tab-' + options.id + '">' + options.title + '</a>' +
            '<span class="glyphicon glyphicon-remove"></span>' +
            '</li>').appendTo($tabWrapper).find('.glyphicon').on('click', function () {
                self.close($label);
            }).end();
        
        var $content = $('<div id="tab-' + options.id + '" class="tab-pane"></div>')
            .appendTo(contentWrapper);
        
        if (options.content) {
            $content.html(options.content);
            options.onLoadSuccess.call(self, $label);
        } else {
            $.ajax({
                url: options.url,
                success: function (html) {
                    $content.html(html);
                    options.onLoadSuccess.call(self, $label);
                }
            });
        }
        
        options.$contentBox = $content;
        $label.data('opts', options);
        $label.find('[href="#tab-' + options.id + '"]').trigger('click');
    };
    
    self.getTabByName = function (name) {
        return $(labelWrapper).children().children('[href="#tab-' + name + '"]').eq(0).parent();
    };
    
    self.reload = function (tab) {
        var options = tab.data('opts');
        if (options) {
            if (options.content) {
                options.$contentBox.html(options.content);
                if (options.onLoadSuccess) {
                    options.onLoadSuccess.call(self, tab);
                }
            } else {
                $.ajax({
                    url: options.url,
                    success: function (html) {
                        options.$contentBox.html(html);
                        if (options.onLoadSuccess) {
                            options.onLoadSuccess.call(self, tab);
                        }
                    }
                });
            }
        } else {
            console.log('该tab非程序所创建，无法刷新！');
        }
    };
    
    self.getContext = function (tab) {
        return $(contentWrapper).children().eq(tab.index());
    };
    
    self.close = function (tab) {
        self.getContext(tab).remove();
        tab.remove();
        if (tab.hasClass('active')) {
            $(labelWrapper).children(":last").children('a').trigger('click');
        }
    };
    
    return self;
}
