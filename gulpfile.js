var gulp = require('gulp'),  
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require('gulp-rename'),
    // jshint = require('gulp-jshint'),
    // uglify = require('gulp-uglify'),
    // imagemin = require('gulp-imagemin'),
    // clean = require('gulp-clean'),
    // concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    // cache = require('gulp-cache'),
    // livereload = require('gulp-livereload'),
    browserSync = require('browser-sync').create();

// 样式
gulp.task('styles', function() {
  return gulp.src('src/styles/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ style: 'expanded'}))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('src/styles'));
    // .pipe(notify({ message: 'Styles task complete' }));
});

// 脚本
// gulp.task('scripts', function() {  
//   return gulp.src('src/scripts/**/*.js')
//     .pipe(jshint('.jshintrc'))
//     .pipe(jshint.reporter('default'))
//     .pipe(concat('main.js'))
//     .pipe(gulp.dest('dist/scripts'))
//     .pipe(rename({ suffix: '.min' }))
//     .pipe(uglify())
//     .pipe(gulp.dest('dist/scripts'))
//     .pipe(notify({ message: 'Scripts task complete' }));
// });

// 图片
// gulp.task('images', function() {  
//   return gulp.src('src/images/**/*')
//     .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
//     .pipe(gulp.dest('dist/images'))
//     .pipe(notify({ message: 'Images task complete' }));
// });

// 清理
// gulp.task('clean', function() {  
//   return gulp.src(['dist/styles', 'dist/scripts', 'dist/images'], {read: false})
//     .pipe(clean());
// });

// 监控
gulp.task('watch', function() {
    // 监控所有.scss档
    gulp.watch('src/styles/**/*.scss', ['styles']);
    // 监控所有.js档
    // gulp.watch('src/scripts/**/*.js', ['scripts']);
    // // 监控所有图片档
    // gulp.watch('src/images/**/*', ['images']);

    // 建立即时重整伺服器
    // var server = livereload();
    // livereload.listen();
    // // 监控所有位在 dist/  目录下的档案，一旦有更动，便进行重整
    // gulp.watch(['dist/**']).on('change', function(file) {
    //     gulp.src(file.path).pipe(livereload());
    // });
});

// 服务
gulp.task('serve',['watch'], function() {
    browserSync.init({
        server: {
            baseDir: ['./'],
            port: 3000
        },
        startPath: '/src'
    });
	gulp.watch(['src/*.html', 'src/views/**/*.html']).on('change', browserSync.reload);
});

// 预设任务
// gulp.task('default', ['clean', 'serve'], function() {  
gulp.task('default', ['serve'], function() {  
    // gulp.start('styles', 'scripts', 'images');
    gulp.start('styles');
});
